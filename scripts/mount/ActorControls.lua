local controlled = false
local self = require('openmw.self')
local util = require('openmw.util')
local types = require('openmw.types')
local attemptJump = false
local startAttack = false
local Actor = types.Actor
local autoMove = false
local storage = require('openmw.storage')
local settingsGroup = 'SettingsFirstPersonMounts'
local globalSettings = storage.globalSection(settingsGroup)
local function MB_processMovement(data)
    controlled = true
    local MFBA = data.MFBA                 --input.CONTROLLER_AXIS.MoveForwardBackward
    local CSM = data.CSM                   --input.getAxisValue(input.CONTROLLER_AXIS.MoveLeftRight)
    local moveLeft = data.moveLeft         -- input.isActionPressed(input.ACTION.MoveLeft )
    local moveRight = data.moveRight        -- input.isActionPressed(input.ACTION.MoveLeft )
    local moveBackward = data.moveBackward -- input.isActionPressed(input.ACTION.MoveLeft )
    local moveForward = data.moveForward   -- input.isActionPressed(input.ACTION.MoveLeft )
    local jumping = data.jumping           --input.getControlSwitch(input.CONTROL_SWITCH.Jumping)
    local sneaking = data.sneaking         -- input.isActionPressed(input.ACTION.Sneak)
    local controllerMovement = MFBA---input.getAxisValue(input.CONTROLLER_AXIS.MoveForwardBackward)
    local controllerSideMovement = CSM--input.getAxisValue(input.CONTROLLER_AXIS.MoveLeftRight)
    local horseRotateSpeed = data.horseRotateSpeed
    local doSprint = data.doSprint
    local run = data.run
    if data.startAutoMove then
        autoMove = true
        print("autoMove")
    end
    if controllerMovement ~= 0 or controllerSideMovement ~= 0 then
        -- controller movement
        if util.vector2(controllerMovement, controllerSideMovement):length2() < 0.25
            and not self.controls.sneak and types.Actor.isOnGround(self) and not types.Actor.isSwimming(self) then
            self.controls.run = false
            self.controls.movement = controllerMovement * 2
            self.controls.sideMovement = controllerSideMovement * 2
        else
            self.controls.run = true
            self.controls.movement = controllerMovement
            self.controls.sideMovement = controllerSideMovement
        end
    else
        --   if(controllerSettings:get("ForceControllerMode") == false) then

        -- keyboard movement
        self.controls.movement = 0
        self.controls.sideMovement = 0
        --local yawChanceAmount = 0.01
        if moveLeft then
            self.controls.yawChange = self.controls.yawChange - horseRotateSpeed
        end
        if moveRight  then
            self.controls.yawChange = self.controls.yawChange + horseRotateSpeed
        end
        if moveBackward then
            self.controls.movement = self.controls.movement - 1
            autoMove = false
        end
        if moveForward then
            self.controls.movement = self.controls.movement + 1
            autoMove = false
        end
        --  end
    end
    if doSprint then
        local modifier = globalSettings:get("mountSprintBonus")
        if not modifier then
            modifier = 60 end
            if  types.Actor.stats.attributes.speed(self).modifier ~= modifier then


        types.Actor.stats.attributes.speed(self).modifier = modifier
            end
        print("RUN",modifier)
    else
        types.Actor.stats.attributes.speed(self).modifier = 0
    end
    if  run == nil then run = false end
    self.controls.run = run--input.isActionPressed(input.ACTION.Run) ~= settings:get('alwaysRun')
    --if self.controls.movement ~= 1 or not types.Actor.canMove(self) then
     --   autoMove = false
    if autoMove then
        self.controls.movement = 1
    end
    self.controls.jump = attemptJump and jumping
    --if not settings:get('toggleSneak') then
        self.controls.sneak = sneaking
  --  end
end
local function MB_setAIState(ai)
self:enableAI(ai)
end
local function MB_processAttacking(data)
    local triggerRight = data.triggerRight --input.getAxisValue(input.CONTROLLER_AXIS.TriggerRight)
    local useAction = data.useAction       --input.isActionPressed(input.ACTION.Use)
    if startAttack then
        self.controls.use = 1
    elseif Actor.stance(self) == Actor.STANCE.Spell then
        self.controls.use = 0
    elseif triggerRight < 0.6
        and not useAction then
        -- The value "0.6" shouldn't exceed the triggering threshold in BindingsManager::actionValueChanged.
        -- TODO: Move more logic from BindingsManager to Lua and consider to make this threshold configurable.
        self.controls.use = 0
    end
end
local function MB_startAttackNow()

    startAttack = Actor.stance(self) ~= Actor.STANCE.Nothing
end

local function MB_readySpell()
    if Actor.stance(self) == Actor.STANCE.Spell then
        Actor.setStance(self, Actor.STANCE.Nothing)
    else
       
            Actor.setStance(self, Actor.STANCE.Spell)
        
    end

end
local function MB_jump()
    controlled = true
attemptJump = true
end
local function MB_toggleWeapon()
    if Actor.stance(self) == Actor.STANCE.Weapon then
        Actor.setStance(self, Actor.STANCE.Nothing)
    else
        Actor.setStance(self, Actor.STANCE.Weapon)
    end
end
return {
    eventHandlers  = {
        MB_processMovement = MB_processMovement,
        MB_processAttacking = MB_processAttacking,
        MB_setAIState = MB_setAIState,
        MB_startAttackNow = MB_startAttackNow,
        MB_readySpell = MB_readySpell,
        MB_toggleWeapon = MB_toggleWeapon,
        MB_jump = MB_jump,
    }
}
