local self = require('openmw.self')
local util = require('openmw.util')
local types = require('openmw.types')
local I = require("openmw.interfaces")
local function onUpdate(dt)
    if I.AI.getActivePackage() ~= nil and I.AI.getActivePackage().type:lower() == "combat" then
        I.AI.removePackages("Combat")
    end
end
return { engineHandlers = { onUpdate = onUpdate } }
