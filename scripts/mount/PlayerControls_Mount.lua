local core = require('openmw.core')
local input = require('openmw.input')
local self = require('openmw.self')
local util = require('openmw.util')
local ui = require('openmw.ui')
local input = require('openmw.input')
local Actor = require('openmw.types').Actor
local Player = require('openmw.types').Player
local debug = require('openmw.debug')
local camera = require('openmw.camera')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')

local settingsGroup = 'SettingsFirstPersonMounts'
local mountMenu = require("scripts.mount.mount_menu")
local controlledActor = self

local mountActor
local playerSettings = storage.playerSection(settingsGroup)

local controlsAllowed = false
local settings = storage.playerSection("LocalSettingsMount")
local isFourtyEight = false
local attemptJump = false
local startAttack = false
local autoMove = false
local movementControlsOverridden = true
local combatControlsOverridden = false
local cbut = input.CONTROLLER_BUTTON

local menuOpen = false

local function setCollisionState(state)
    if isFourtyEight then return end
    if debug.isCollisionEnabled() ~= state then
        debug.toggleCollision()
    end
end
local function processMovement(data)
    local MFBA = data.MFBA                 --input.CONTROLLER_AXIS.MoveForwardBackward
    local CSM = data.CSM                   --input.getAxisValue(input.CONTROLLER_AXIS.MoveLeftRight)
    local moveLeft = data.moveLeft         -- input.isActionPressed(input.ACTION.MoveLeft )
    local moveRight = data.moveRight       -- input.isActionPressed(input.ACTION.MoveLeft )
    local moveBackward = data.moveBackward -- input.isActionPressed(input.ACTION.MoveLeft )
    local moveForward = data.moveForward   -- input.isActionPressed(input.ACTION.MoveLeft )
    local jumping = data.jumping           --input.getControlSwitch(input.CONTROL_SWITCH.Jumping)
    local sneaking = data.sneaking         -- input.isActionPressed(input.ACTION.Sneak)
    local controllerMovement = MFBA        ---input.getAxisValue(input.CONTROLLER_AXIS.MoveForwardBackward)
    local controllerSideMovement = CSM     --input.getAxisValue(input.CONTROLLER_AXIS.MoveLeftRight)
    if controllerMovement ~= 0 or controllerSideMovement ~= 0 then
        -- controller movement
        if util.vector2(controllerMovement, controllerSideMovement):length2() < 0.25
            and not controlledActor.controls.sneak and Actor.isOnGround(controlledActor) and not Actor.isSwimming(controlledActor) then
            controlledActor.controls.run = false
            controlledActor.controls.movement = controllerMovement * 2
            controlledActor.controls.sideMovement = controllerSideMovement * 2
        else
            controlledActor.controls.run = true
            controlledActor.controls.movement = controllerMovement
            controlledActor.controls.sideMovement = controllerSideMovement
        end
    else
        --   if(controllerSettings:get("ForceControllerMode") == false) then

        -- keyboard movement
        controlledActor.controls.movement = 0
        controlledActor.controls.sideMovement = 0
        if moveLeft and input.isControllerButtonPressed(cbut.DPadLeft) == false then
            controlledActor.controls.sideMovement = controlledActor.controls.sideMovement - 1
        end
        if moveRight and input.isControllerButtonPressed(cbut.DPadRight) == false then
            controlledActor.controls.sideMovement = controlledActor.controls.sideMovement + 1
        end
        if moveBackward and input.isControllerButtonPressed(cbut.DPadDown) == false then
            controlledActor.controls.movement = controlledActor.controls.movement - 1
        end
        if moveForward and input.isControllerButtonPressed(cbut.DPadUp) == false then
            controlledActor.controls.movement = controlledActor.controls.movement + 1
        end
        controlledActor.controls.run = true --input.isActionPressed(input.ACTION.Run) ~= settings:get('alwaysRun')
        --  end
    end
    if controlledActor.controls.movement ~= 0 or not Actor.canMove(controlledActor) then
        autoMove = false
    elseif autoMove then
        controlledActor.controls.movement = 1
    end
    controlledActor.controls.jump = attemptJump and jumping
    if not settings:get('toggleSneak') then
        -- controlledActor.controls.sneak = sneaking
    end
end

local function processAttacking(data)
    local triggerRight = data.triggerRight --input.getAxisValue(input.CONTROLLER_AXIS.TriggerRight)
    local useAction = input.isActionPressed(input.ACTION.Use)
    if startAttack then
        self.controls.use = 1
        startAttack = false
    elseif Actor.stance(self) == Actor.STANCE.Spell then
        self.controls.use = 0
    elseif triggerRight < 0.6
        and not useAction then
        -- The value "0.6" shouldn't exceed the triggering threshold in BindingsManager::actionValueChanged.
        -- TODO: Move more logic from BindingsManager to Lua and consider to make this threshold configurable.
        self.controls.use = 0
    end
end

local function onFrame(dt)
    if (combatControlsOverridden) then
        return
    end
    controlsAllowed = not core.isWorldPaused()
    if not movementControlsOverridden then
        if controlsAllowed then
            local MFBA = -input.getAxisValue(input.CONTROLLER_AXIS.MoveForwardBackward)
            local CSM = input.getAxisValue(input.CONTROLLER_AXIS.MoveLeftRight)
            local moveLeft = input.isActionPressed(input.ACTION.MoveLeft)
            local moveRight = input.isActionPressed(input.ACTION.MoveRight)
            local moveBackward = input.isActionPressed(input.ACTION.MoveBackward)
            local moveForward = input.isActionPressed(input.ACTION.MoveForward)
            local jumping = input.getControlSwitch(input.CONTROL_SWITCH.Jumping)
            local sneaking = input.isActionPressed(input.ACTION.Sneak)
            local run = input.isActionPressed(input.ACTION.AlwaysRun)
            if (controlledActor.id == self.id) then
                processMovement({
                    CSM = CSM,
                    MFBA = MFBA,
                    moveLeft = moveLeft,
                    moveRight = moveRight,
                    moveBackward = moveBackward,
                    moveForward = moveForward,
                    jumping = jumping,
                    sneaking = sneaking
                })
            else
                controlledActor:sendEvent("MB_processMovement",
                    {
                        CSM = CSM,
                        MFBA = MFBA,
                        moveLeft = moveLeft,
                        moveRight = moveRight,
                        moveBackward = moveBackward,
                        moveForward = moveForward,
                        jumping = jumping,
                        sneaking = sneaking,
                        run = settings:get('alwaysRun'),
                        startAutoMove = input.isActionPressed(input.ACTION.AutoMove),
                        doSprint = input.isActionPressed(input.ACTION.Run),
                        horseRotateSpeed = playerSettings:get("horseRotateSpeed")
                    })
            end
        else
            self.controls.movement = 0
            self.controls.sideMovement = 0
            self.controls.jump = false
        end
    end
    local triggerRight = input.getAxisValue(input.CONTROLLER_AXIS.TriggerRight)
    processAttacking({ triggerRight = triggerRight, useAction = useAction })
    if controlsAllowed then
        local useAction = input.isActionPressed(input.ACTION.Use)
    elseif (combatControlsOverridden == false) then
        local triggerRight = input.getAxisValue(input.CONTROLLER_AXIS.TriggerRight)
        local useAction = input.isActionPressed(input.ACTION.Use)
        if (useAction) then
            controlledActor:sendEvent("MB_processAttacking", { triggerRight = triggerRight, useAction = useAction })
        end
    end
    attemptJump = false
end
local function toggleSneak(sneak)
    self.controls.sneak = not self.controls.sneak
end
local function takeControlOfActor(actor, force)
    if force == nil then
        force = false
    end
    input.setControlSwitch(input.CONTROL_SWITCH.VanityMode, actor.id == self.id)
    input.setControlSwitch(input.CONTROL_SWITCH.ViewMode, actor.id == self.id)
    setCollisionState(actor.id == self.id)
    if (actor.id == self.id) then
        core.sendGlobalEvent("setAnchorObject")
        I.Controls_mount.overrideMovementControls(true)
        I.Controls_mount.overrideCombatControls(true)
        if I.Controls then
            I.Controls.overrideMovementControls(false)
            I.Controls.overrideCombatControls(false)
        end
        --   I.SlaveScript.setCameraTarget(self)
        I.Controls_mount.setTargetActor(self)
    else
        camera.setMode(camera.MODE.FirstPerson)
        -- setCollisionState(true)
        core.sendGlobalEvent("setAnchorObject", { anchorObject = actor, zAmount = 100 })
        I.Controls_mount.overrideMovementControls(false)
        I.Controls_mount.overrideCombatControls(false)
        if I.Controls then
            I.Controls.overrideMovementControls(true)
            I.Controls.overrideCombatControls(true)
        end
        if not playerSettings:get("allowWeapon") then
            Actor.setStance(self, Actor.STANCE.Nothing)
        end
        --   I.SlaveScript.setCameraTarget(actor)
        I.Controls_mount.setTargetActor(actor)
        actor:sendEvent("MB_setAIState", false)
    end
end
local function talkToMount(actor)
    if not actor then actor = mountActor end

    if not actor then return end
    if actor.recordId == self.recordId then return end

    mountMenu.showMessageBox(actor)
    menuOpen = true
   -- I.UI.setMode(I.UI.MODE.Dialogue, { target = actor })
end
local function takeControlOfActorEvent(data)
    if data.actor and data.actor.id ~= self.id then
        mountActor = data.actor
    end
    if data.force == true then
        takeControlOfActor(data.actor, data.force)
        return
    end
    if self.controls.sneak then
        local activateAction = playerSettings:get("mountSneakActivateAction")
        if activateAction == "Mount/Dismount" then
            takeControlOfActor(data.actor, data.force)
        elseif activateAction == "Speak To" then
            talkToMount(data.actor)
        elseif activateAction == "Open Inventory" then
            I.UI.setMode(I.UI.MODE.Companion, { target = data.actor })
        end
        return
    else
        local activateAction = playerSettings:get("mountActivateAction")
        if activateAction == "Mount/Dismount" then
            takeControlOfActor(data.actor, data.force)
        elseif activateAction == "Speak To" then
            talkToMount(data.actor)
        elseif activateAction == "Open Inventory" then
            I.UI.setMode(I.UI.MODE.Companion, { target = data.actor })
        end
        return
    end
end
local function startAttackNow()
    startAttack = Actor.stance(self) ~= Actor.STANCE.Nothing
end
local function onInputAction(action)
    if core.isWorldPaused() then
        return
    end

    if action == input.ACTION.Jump then
        if (controlledActor.id ~= self.id) then
            controlledActor:sendEvent(" MB_jump")
        else
            attemptJump = true
        end
    elseif action == input.ACTION.Use then
        if (controlledActor.id ~= self.id) then
            --controlledActor:sendEvent("MB_startAttackNow")
            startAttackNow()
        else
            startAttackNow()
        end
    elseif action == input.ACTION.AutoMove and not movementControlsOverridden then
        autoMove = not autoMove
    elseif action == input.ACTION.AlwaysRun and not movementControlsOverridden then
        settings:set('alwaysRun', not settings:get('alwaysRun'))
    elseif action == input.ACTION.Sneak and not movementControlsOverridden then
        if settings:get('toggleSneak') then
            toggleSneak()
        end
    elseif action == input.ACTION.ToggleSpell and playerSettings:get("allowWeapon") then
        if (controlledActor.id ~= self.id and true == false) then
            controlledActor:sendEvent("MB_readySpell")
        else
            if Actor.stance(self) == Actor.STANCE.Spell then
                Actor.setStance(self, Actor.STANCE.Nothing)
            elseif input.getControlSwitch(input.CONTROL_SWITCH.Magic) then
                if Player.isWerewolf(self) then
                    ui.showMessage(core.getGMST('sWerewolfRefusal'))
                else
                    Actor.setStance(self, Actor.STANCE.Spell)
                end
            end
        end
    elseif action == input.ACTION.ToggleWeapon and playerSettings:get("allowWeapon") then
        if (controlledActor.id ~= self.id and true == false) then
            controlledActor:sendEvent(" MB_toggleWeapon")
        else
            if Actor.stance(self) == Actor.STANCE.Weapon then
                Actor.setStance(self, Actor.STANCE.Nothing)
            else
                Actor.setStance(self, Actor.STANCE.Weapon)
            end
        end
    end
end
local function setTargetActor(actor)
    controlledActor = actor
end
local function onKeyPress(key)
    if menuOpen then
        mountMenu.keyPress(key)
        return
    end
    if (key.code == input.KEY.LeftShift or key.code == input.KEY.RightShift) then
        core.sendGlobalEvent("MountShiftUpdate", true)
    elseif controlledActor == self and mountActor ~= nil and key.symbol == playerSettings:get("mountBinding") then
        -- core.sendGlobalEvent("HorseActivated")
        takeControlOfActor(mountActor, true)
    elseif mountActor ~= nil and controlledActor == mountActor and key.symbol == playerSettings:get("mountBinding") then
        core.sendGlobalEvent("HorseActivated", true)
        -- takeControlOfActor(self, true)
    end
end
local function onKeyRelease(key)
    if (key.code == input.KEY.LeftShift or key.code == input.KEY.RightShift) then
        core.sendGlobalEvent("MountShiftUpdate", false)
    end
end
local buttons = {"Companion Share","Mount","Follow","Wait","Wander","Cancel"}
local function mountMenuOptionClicked(text)
    menuOpen = false
if text == "Companion Share" then
    I.UI.setMode(I.UI.MODE.Companion, { target = mountActor })
elseif text == "Mount" then
    takeControlOfActor(mountActor,true)
elseif text == "Follow" then
    mountActor:sendEvent('StartAIPackage', {type='Follow', target=self.object})
elseif text == "Wait" then
    mountActor:sendEvent('StartAIPackage', {type='Wander'})
elseif text == "Wander" then
    mountActor:sendEvent('StartAIPackage', {type='Wander',distance = 1000})
end
end
return {
    engineHandlers = {
        onFrame = onFrame,
        onInputAction = onInputAction,
        onKeyRelease = onKeyRelease,
        onKeyPress = onKeyPress,
    },
    eventHandlers = {
        MB_processAttacking = processAttacking,
        takeControlOfActorEvent = takeControlOfActorEvent,
        mountMenuOptionClicked = mountMenuOptionClicked,

        MB_OpenInv = function(actor)
            I.UI.setMode(I.UI.MODE.Companion, { target = actor })
        end
    },
    interfaceName = 'Controls_mount',
    ---
    -- @module Controls
    -- @usage require('openmw.interfaces').Controls
    interface = {
        takeControlOfActor = takeControlOfActor,
        --- Interface version
        -- @field [parent=#Controls] #number version
        version = 0,
        setTargetActor = function(a) controlledActor = a end,

        --- When set to true then the movement controls including jump and sneak are not processed and can be handled by another script.
        -- If movement should be dissallowed completely, consider to use `input.setControlSwitch` instead.
        -- @function [parent=#Controls] overrideMovementControls
        -- @param #boolean value
        overrideMovementControls = function(v) movementControlsOverridden = v end,

        --- When set to true then the controls "attack", "toggle spell", "toggle weapon" are not processed and can be handled by another script.
        -- If combat should be dissallowed completely, consider to use `input.setControlSwitch` instead.
        -- @function [parent=#Controls] overrideCombatControls
        -- @param #boolean value
        overrideCombatControls = function(v) combatControlsOverridden = v end,
    }
}
