local core = require('openmw.core')
local input = require('openmw.input')
local self = require('openmw.self')
local util = require('openmw.util')
local ui = require('openmw.ui')
local input = require('openmw.input')
local Actor = require('openmw.types').Actor
local Player = require('openmw.types').Player
local debug = require('openmw.debug')
local camera = require('openmw.camera')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')
local mountMenu = require("scripts.mount.mount_menu")
local function onQuestUpdate(questId,stage)

if questId == "zhac_horseq" and stage == 2 then
core.sendGlobalEvent("HorseOwnershipSet",true)
end
end

return {engineHandlers = {onQuestUpdate = onQuestUpdate}}