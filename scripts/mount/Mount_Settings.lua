local I = require("openmw.interfaces")
local storage = require("openmw.storage")
local async = require("openmw.async")
local core = require("openmw.core")

local settingsGroup = 'SettingsFirstPersonMounts'
local playerSettings = storage.playerSection(settingsGroup)
I.Settings.registerPage {
     key = "FirstPersonMounts",
     l10n = "FirstPersonMounts",
     name = "First Person Mounts",
     description = "Settings for First Person Mounts"
}
I.Settings.registerGroup {
     key = "SettingsFirstPersonMounts",
     page = "FirstPersonMounts",
     l10n = "FirstPersonMounts",
     name = "First Person Mounts Settings",
     description = "Each line below will be ran as a command when the console enters global context.",
     permanentStorage = true,
     settings = {

          {
               key = "mountBinding",
               renderer = "textLine",
               name = "Key to press to mount(or call) the last mountable actor",
               description = "When nearby, this key will mount the last ridden Horse or guar. When it is far, it will request it come near to you.",
               default = "k"
          },

          {
               key = "horseRotateSpeed",
               renderer = "number",
               name = "Mount Rotate Speed",
               description = "This is the speed the mount you are on will rotate when you press the corrosponding key.",
               default = 0.02
          },
          {
               key = "allowWeapon",
               renderer = "checkbox",
               name = "Allow Weapon and Spells on Mounts",
               description = "If true, you will be able to draw your Weapon or Spells while riding a mount. This can cause issues where you attack your mount, so be careful.",
               default = false
          },
          {
               key = "mountActivateAction",
               renderer = "select",
               name = "Action for Activating Mount",
               description = "Allows you to specify if you will mount, speak to, or open the inventory of your mount when activating it.",
               default = "Speak To",
               argument = {
                   disabled = false,
                   items = {"Mount/Dismount","Speak To","Open Inventory"},
                   l10n = "FirstPersonMounts",
               },
          },
          {
               key = "mountSneakActivateAction",
               renderer = "select",
               name = "Action for Sneak Activating Mount",
               description = "Allows you to specify if you will mount, speak to, or open the inventory of your mount when sneak-activating it.",
               default = "Mount/Dismount",
               argument = {
                   disabled = false,
                   items = {"Mount/Dismount","Speak To","Open Inventory"},
                   l10n = "FirstPersonMounts",
               },
          },
          {
               key = "mountSpeedModifier",
               renderer = "number",
               name = "Speed Bonus for Mount",
               description = "When set, mounts will have the player's speed, plus this amount.",
               default = 40,
          },
          {
               key = "mountSprintBonus",
               renderer = "number",
               name = "Speed Bonus for Sprint",
               description = "When set, mounts will have this much speed bonus when sprinting.",
               default = 40,
          },

     },

}

local function updateKeyset(section, key)
     if key then
         core.sendGlobalEvent("Mount_SettingUpdate",
             { key = key, value = storage.playerSection(section):get(key), section = section })
     end
 end
 playerSettings:subscribe(async:callback(updateKeyset))