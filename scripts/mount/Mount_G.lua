local types = require("openmw.types")
local acti = require("openmw.interfaces").Activation
local core = require("openmw.core")
local world = require("openmw.world")
local util = require("openmw.util")
local I = require("openmw.interfaces")
local storage = require("openmw.storage")

local anchorObject
local zAmount = 0
local playerLastRot
local settingsGroup = 'SettingsFirstPersonMounts'
local globalSettings = storage.globalSection(settingsGroup)

local horseRecordId = "zhac_horse_01"
local guarRecordId = "zhac_mount_guar"
local nixRecordId = "zhac_mount_nix"
local zamountMap = { [horseRecordId] = 100, [guarRecordId] = 130, [nixRecordId] = 100 }
local ridableActors = {}
local horseCreated = false
local shiftPressed = false
local horseOwned = false

local horseControlled = false
local horseObject = nil
local horseId
local anchorLastRot

local bridgeLastCell = "ZHAC_mount_holdingcell"
local bridgeLastCellX = 0
local bridgeLastCellY = 0

local bridgeObject

local function getBridgeObject()
    if not bridgeObject then
        if bridgeLastCell ~= "" and bridgeLastCell ~= nil then
            for index, value in ipairs(world.getCellByName(bridgeLastCell):getAll(types.Activator)) do
                if value.recordId == "zhac_mount_playerpositioner" then
                    bridgeObject = value
                    return
                end
            end
        else
            for index, value in ipairs(world.getExteriorCell(bridgeLastCellX, bridgeLastCellY):getAll(types.Activator)) do
                if value.recordId == "zhac_mount_playerpositioner" then
                    bridgeObject = value
                    return
                end
            end
        end
    end
end
local function addRidableActor(actor)
    ridableActors[actor.id] = true
end
local function getPlayer()
    for i, ref in ipairs(world.activeActors) do
        if (ref.type == types.Player) then
            return ref
        end
    end
end
local isFourtyEight = false
local function setPlayerPosition(pos, rotZ)
    if isFourtyEight then
        local cell = horseObject.cell
        bridgeLastCellX = cell.gridX
        bridgeLastCellY = cell.gridY
        if not bridgeObject then
            getBridgeObject()
        end

        bridgeObject:teleport(cell, horseObject.position)
        bridgeLastCell = ""
    else
        local scr = world.mwscript.getGlobalScript("zhac_positionscr", getPlayer())
        scr.variables.px = pos.x
        scr.variables.py = pos.y
        scr.variables.pz = pos.z
        scr.variables.pr = math.deg(rotZ)
        scr.variables.domove = 1
    end
end
local function onUpdate()
    if anchorObject then
        local player = getPlayer()
        local anchorObjectPos = anchorObject.position
        local playerRot = player.rotation:getAnglesZYX()
        local anchorRot = anchorObject.rotation:getAnglesZYX()
        if not anchorLastRot then
            playerRot = anchorRot
            anchorLastRot = anchorRot
        end
        local rdelta = anchorLastRot - anchorRot
        local tpPos = util.vector3(anchorObjectPos.x + 0, anchorObjectPos.y + 0, anchorObjectPos.z + zAmount)
        --world.players[1]:teleport(anchorObject.cell, tpPos)
        setPlayerPosition(tpPos, playerRot - rdelta)
        anchorLastRot = anchorRot
        playerLastRot = playerRot
    end
end
local horseStartingPos = util.vector3(93853.9921875, 121271.046875, 1353.615478515625)
local guarStartingPos = util.vector3(-66372.328125, 140336.390625, 92.1305389404296875)
local nixStartingPos = util.vector3(-8434.279, 19876.576, 1138.313)
local function onPlayerAdded()
    if not horseCreated then
        for index, value in ipairs(world.getCellByName("ZHAC_mount_holdingcell"):getAll(types.Creature)) do
            if value.recordId == "zhac_horse_01" then
                horseObject = value
                break
            end
        end
        if not horseObject then
            error("Unable to find the horse object. Make sure the omwaddon for First Person Mounts is loaded.")
        end
        -- horseObject = world.createObject(horseRecordId)
        horseObject:teleport("", horseStartingPos)
        -- horseId = horseObject.id
        horseObject:addScript('scripts/mount/Mount_Actor_scr.lua')
        addRidableActor(horseObject)
        local guarObject = world.createObject(guarRecordId)
        guarObject:teleport("", guarStartingPos)
        --horseId = horseObject.id
        addRidableActor(guarObject)
        local nixObject = world.createObject(nixRecordId)
        nixObject:teleport("", nixStartingPos)
        --horseId = horseObject.id
        addRidableActor(nixObject)
        horseCreated = true
    end
end

local function onSave()
    return { horseId = horseId, horseControlled = horseControlled, ridableActors = ridableActors, horseOwned = horseOwned }
end
local function onLoad(data)
    if not data then return end
    horseId = data.horseId
    horseCreated = true
    horseControlled = data.horseControlled
    ridableActors = data.ridableActors
    horseOwned = data.horseOwned
    for index, value in ipairs(world.activeActors) do
        if value.id == horseId then
            horseObject = value
        end
    end
    if horseControlled and horseObject then
        getPlayer():sendEvent("takeControlOfActorEvent", { actor = horseObject, force = true })
    end
end
local function getPositionBehind(pos, rot, distance, direction)
    local currentRotation = -rot
    local angleOffset = 0

    if direction == "north" then
        angleOffset = math.rad(90)
    elseif direction == "south" then
        angleOffset = math.rad(-90)
    elseif direction == "east" then
        angleOffset = 0
    elseif direction == "west" then
        angleOffset = math.rad(180)
    else
        error("Invalid direction. Please specify 'north', 'south', 'east', or 'west'.")
    end

    currentRotation = currentRotation - angleOffset
    local obj_x_offset = distance * math.cos(currentRotation)
    local obj_y_offset = distance * math.sin(currentRotation)
    local obj_x_position = pos.x + obj_x_offset
    local obj_y_position = pos.y + obj_y_offset
    return util.vector3(obj_x_position, obj_y_position, pos.z)
end
local function HorseActivated(force)
    local actor = getPlayer()
    local object = horseObject
    if not object then
        for index, value in ipairs(actor.cell:getAll(types.Creature)) do
            if ridableActors[value.id] then
                horseObject = value
            end
        end
    end
    if not object then return end
    if zamountMap[object.recordId] then
        zAmount = zamountMap[object.recordId]
    else
        zAmount = 100
    end
    if ridableActors[object.id] then
        if not horseObject or object.id ~= horseId then
            horseObject = object
            if object.id ~= horseId then
                horseId = object.id
            end
        end
        if horseControlled and anchorObject then
            getPlayer():sendEvent("takeControlOfActorEvent", { actor = actor, force = true })
            actor:teleport(actor.cell,
                getPositionBehind(anchorObject.position, anchorObject.rotation:getAnglesZYX(), 100, "west"))
            anchorObject:sendEvent("MB_setAIState", true)
            anchorObject = nil
            anchorLastRot = nil
        else
            getPlayer():sendEvent("takeControlOfActorEvent", { actor = horseObject, force = false })
        end
    end
end
local function PlayerPosUpdate(data)
    local player = data.player
    local ref = data.ref
    player:teleport(ref.cell, util.vector3(ref.position.x, ref.position.y, ref.position.z - 000))
end
local function activateCreature(object, actor)
    print(object.recordId)
    if not shiftPressed or horseId ~= object.id and types.Actor.stats.dynamic.health(object).current > 0 then
        if ridableActors[object.id] then
            if object.recordId == horseRecordId:lower() and not horseOwned then
                return false
            end
            horseId = object.id
            horseObject = object
            HorseActivated()
            return false
        end
    else
    end
end
local function Mount_SettingUpdate(data)
    globalSettings:set(data.key, data.value)
end
local function HorseOwnershipSet(state)
    horseOwned = state
end
acti.addHandlerForType(types.Creature, activateCreature)
return {
    engineHandlers = { onUpdate = onUpdate, onPlayerAdded = onPlayerAdded, onSave = onSave, onLoad = onLoad },
    eventHandlers = {
        HorseActivated      = HorseActivated,
        PlayerPosUpdate     = PlayerPosUpdate,
        HorseOwnershipSet   = HorseOwnershipSet,
        Mount_SettingUpdate = Mount_SettingUpdate,
        addRidableActor     = addRidableActor,
        setAnchorObject     = function(data)
            if not data then
                anchorObject = nil
                horseControlled = false
                return
            end
            anchorObject = data.anchorObject
            horseControlled = true
            if data.anchorObject then
                if zamountMap[data.anchorObject.recordId] then
                    zAmount = zamountMap[data.anchorObject.recordId]
                else
                    zAmount = 100
                end
            end
        end,
        MountShiftUpdate    = function(state) shiftPressed = state end
    }
}
