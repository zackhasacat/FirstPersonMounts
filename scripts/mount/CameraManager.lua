local core = require "openmw.core"
local validCommands = { ["tfc"] = true, ["freecam"] = true, ["luatfc"] = true, ["luafreecam"] = true }
local input = require("openmw.input")
local camera = require('openmw.camera')
local util = require('openmw.util')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local types = require('openmw.types')
local aux = require("scripts.LFC.lfc_auxp")
local mult = 0.01

local active = false
local anchorObject
local zAmount = 100 + 100
local function updateCameraPosition()
    local tpPos = util.vector3(anchorObject.position.x + 0, anchorObject.position.y + 0,
        anchorObject.position.z + zAmount)
    camera.setStaticPosition(tpPos)
end


local function distanceBetweenPos(vector1, vector2)
    --Quick way to find out the distance between two vectors.
    --Very similar to getdistance in mwscript
    local dx = vector2.x - vector1.x
    local dy = vector2.y - vector1.y
    return math.sqrt(dx * dx + dy * dy )
end
local function onUpdate(dt)
    if active then
    camera.setPitch(camera.getPitch() + (input.getMouseMoveY() * mult))
    camera.setYaw(camera.getYaw() + (input.getMouseMoveX() * mult))
        updateCameraPosition()
        if distanceBetweenPos(self.position,anchorObject.position) > 1000 then
            core.sendGlobalEvent("PlayerPosUpdate",{player = self.object,ref = anchorObject})
        end
    end
end
local function takeControlOfActorEvent(anch)
    if not anch then
        camera.setMode(camera.MODE.FirstPerson)
        active = false
        return
    end
    anchorObject = anch.actor
    camera.setMode(camera.MODE.Static)
    active = true
end
return {
    engineHandlers = { onUpdate = onUpdate, },
    eventHandlers = {
 --       takeControlOfActorEvent = takeControlOfActorEvent,
    }
}
